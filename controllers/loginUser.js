const bcrypt = require('bcrypt')
const User = require('../database/models/User')
 
module.exports = (req, res) => {
    const {
        email,
        password
    } = req.body;
    // trouver l'utilisateur
    User.findOne({
        email
    }, (error, user) => {
        if (user) {
            // comparer le mp
            bcrypt.compare(password, user.password, (error, same) => {
                if (same) {
                   req.session.userId = user._id // session utilisateur.
                    res.redirect('/')
                } else {
                    res.redirect('/login')
                }
            })
        } else {
            return res.redirect('/login')
        }
    })
}